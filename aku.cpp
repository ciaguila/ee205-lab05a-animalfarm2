///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @todo Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float weight, enum Color newColor, enum Gender newGender ) {
   gender = newGender;     
   species = "Katsuwonus pelamis";
   scaleColor = newColor;
   favoriteTemp = 75;      
   weight1 = weight;         /// This weight is passed from the constructor to in-house weight1 holder, which is passed to the printInfo function

}


void Aku::printInfo() {
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight1 << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm


