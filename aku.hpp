///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @todo Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Aku : public Fish {
public:
   float weight1; //This float is generated to pass data between the constructor and the printInfo

   Aku( float weight, enum Color newColor, enum Gender newGender ); // constructor for Aku with weight float

   void printInfo();
};

} // namespace animalfarm


