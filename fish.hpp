///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author @todo Christopher Aguilar ciaguila@hawaii.edu
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo Feb 2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
   enum Color scaleColor; // generates an enum for the scale color variable
   float favoriteTemp; // generates a place within the Fish class to define favorite temperature
   
    virtual const string speak();

   void printInfo();
};

} // namespace animalfarm
